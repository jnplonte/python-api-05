# simple application creation using containers

[![license](https://img.shields.io/github/license/mashape/apistatus.svg)]()

## Version

**v1.0.0**

## Dependencies

- python [https://www.python.org/](https://www.python.org/)
- flask: [https://pypi.org/project/Flask/](https://pypi.org/project/Flask/)

## PYTHON

### Installation

- go to python-app `cd python-app`
- create virtualenv `virtualenv -p python python-env`
- work on virtualenv `source python-env/bin/activate`
- install python dependencies by running `pip install -r requirements.txt`

### How to Use

- run `uwsgi app.ini` it will listen to http://localhost:8080

## DOCKER

### Installation

- build `docker-compose build`

### How to Use

- run `docker-compose up`
