from flask import Flask
from flask import jsonify
from flask import request


import requests

# ---------------------------------------------------------------------------------------------------------------------

app = Flask(__name__)

@app.after_request
def corsCheck(response):
    response.headers.add('Access-Control-Allow-Origin', '*')
    response.headers.add('Access-Control-Allow-Headers', '*')
    response.headers.add('Access-Control-Allow-Methods', '*')

    return response


@app.errorhandler(404)
def not_found(error):
    response = jsonify({'status': 'failed', 'error': 'page not found'})
    response.status_code = 404

    return response


@app.route("/")
def url_root():
    publicIp = requests.get('http://ipinfo.io/json').json()['ip']

    response = jsonify({"status": "success", 'data': 'you are connecting from ' + publicIp})
    return response


if __name__ == "__main__":
	app.run(debug=True)